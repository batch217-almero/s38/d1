const jwt = require("jsonwebtoken");

// Secret code to prevent hackings 
const secret = "CourseBookingAPI"

// create access token na kapag na nakakaresume yung email and pasword sa login
module.exports.createAccessToken = (user) => {
	const data = {
		id: user._id,
		email: user.email,
		isAdmin: user.isAdmin
	}

	return jwt.sign(data, secret, {});
};