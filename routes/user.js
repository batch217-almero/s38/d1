const express = require("express");
const router = express.Router();

const User =  require("../models/User.js");
const userController =  require("../controllers/userController.js");


router.post("/checkEmail", (request, response) => {
	userController.checkEmailExists(request.body)
	.then(resultFromController =>response.send(resultFromController)); 
});


router.post("/register", (request, response) => {
	userController.registerUser(request.body)
	.then(resultFromController => response.send(resultFromController));
});


router.post("/login", (request, response) => {
	userController.loginUser(request.body)
	.then(resultFromController => response.send(resultFromController));
});

// S38 Activity - Code Along
router.post("/details", (request, response) => {
	userController.getProfile({userId : request.body.id})
	.then(resultFromController => response.send(resultFromController));
})


module.exports = router;